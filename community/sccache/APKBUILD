# Maintainer: omni <omni+alpine@hack.org>
pkgname=sccache
pkgver=0.7.3
pkgrel=0
pkgdesc="shared compilation cache for C/C++ and Rust"
url="https://github.com/mozilla/sccache/"
# s390x & riscv64: limited by cargo
# ppc64le: not supported by ring crate
# armhf: sigbus
arch="all !s390x !ppc64le !armhf !riscv64"
license="Apache-2.0"
makedepends="cargo openssl-dev>3 cargo-auditable"
subpackages="$pkgname-doc"
source="$pkgname-$pkgver.tar.gz::https://github.com/mozilla/sccache/archive/v$pkgver.tar.gz
	disable_test_s3_invalid_args_patch
	test_musl_ldd_parse.patch
	"

case "$CARCH" in
x86_64)
	# only x86_64 supports sccache-dist
	subpackages="$subpackages $pkgname-dist"
	_features="all,dist-server"
	;;
*)
	_features="all"
	;;
esac


prepare() {
	default_prepare
	cargo fetch --locked
}

build() {
	cargo auditable build --release --frozen --features $_features
}

check() {
	case "$CARCH" in
	armv7|x86)
		patch tests/sccache_args.rs \
			"$srcdir"/disable_test_s3_invalid_args_patch
		;;
	esac

	cargo test --frozen
}

package() {
	install -Dm0755 target/release/sccache -t "$pkgdir"/usr/bin

	case "$CARCH" in
	x86_64)
		install -Dm0755 target/release/sccache-dist \
			-t "$pkgdir"/usr/bin
		;;
	esac

	install -Dm0644 -t "$pkgdir"/usr/share/doc/"$pkgname" \
		docs/Distributed.md \
		docs/DistributedQuickstart.md \
		docs/Jenkins.md \
		docs/Rust.md
}

dist() {
	pkgdesc="$pkgdesc (dist server)"

	amove usr/bin/sccache-dist
}

sha512sums="
1f5d4ffd8771ae53ea5b8176f633dffc2c997328748ed65e8b000899a58cd42e897aa7f0c42f3e89f0732fe881fdc7b193bade64bf9b7f9ceeebdf008440a1a5  sccache-0.7.3.tar.gz
11d4057a79e3a749b1c9d6a157b199a9b4aaac224fa3326f8086ac31d00f791c0c7cb659b1e79be9835b89921722f7e60aeb845786fbb05f59d4fad116cf0fcf  disable_test_s3_invalid_args_patch
b9d7c24a2ae65d066ed3c5a23804be7c3580b49d6b45c7d403c38182bce048225d818431098c33d8945ff1a5549d8505c19bb53860c939bc9f2d7073042f5ce6  test_musl_ldd_parse.patch
"
