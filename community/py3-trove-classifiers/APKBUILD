# Contributor: Michał Polański <michal@polanski.me>
# Maintainer: Michał Polański <michal@polanski.me>
pkgname=py3-trove-classifiers
pkgver=2023.11.14
pkgrel=0
pkgdesc="Canonical source for classifiers on PyPI"
url="https://github.com/pypa/trove-classifiers"
license="Apache-2.0"
arch="noarch"
depends="python3"
makedepends="py3-calver py3-gpep517 py3-installer py3-setuptools py3-wheel"
checkdepends="py3-pytest"
subpackages="$pkgname-pyc"
source="https://github.com/pypa/trove-classifiers/archive/$pkgver/py3-trove-classifiers-$pkgver.tar.gz"
builddir="$srcdir/trove-classifiers-$pkgver"

prepare() {
	default_prepare

	echo "Version: $pkgver" > PKG-INFO
}

build() {
	gpep517 build-wheel \
		--wheel-dir .dist \
		--output-fd 3 3>&1 >&2
}

check() {
	pytest
}

package() {
	python3 -m installer -d "$pkgdir" \
		.dist/trove_classifiers-$pkgver-py3-none-any.whl
}

sha512sums="
5561b3410d0d923f43bf90248e25e5ae9b62bc58038572ea2527d299c9fcbe58ba7cc52495cc28f63e1ba4e7c50275f2f259cff9bd65343154c7e3cb67958537  py3-trove-classifiers-2023.11.14.tar.gz
"
