# Contributor: Kevin Daudt <kdaudt@alpinelinux.org>
# Maintainer: Kevin Daudt <kdaudt@alpinelinux.org>
pkgname=opentofu
pkgver=1.6.0_alpha5
pkgrel=0
pkgdesc="OpenTofu lets you declaratively manage your cloud infrastructure"
url="https://opentofu.org"
# x86, armhf, armv7: several tests fail
arch="all !x86 !armhf !armv7"
license="MPL-2.0"
makedepends="go"
source="$pkgname-$pkgver.tar.gz::https://github.com/opentofu/opentofu/archive/refs/tags/v${pkgver/_/-}.tar.gz"
builddir="$srcdir/$pkgname-${pkgver/_/-}"

export GOCACHE="${GOCACHE:-"$srcdir/go-cache"}"
export GOTMPDIR="${GOTMPDIR:-"$srcdir"}"
export GOMODCACHE="${GOMODCACHE:-"$srcdir/go"}"

build() {
	local ldflags="-X 'github.com/opentofu/opentofu/version.dev=no'"
	go build -ldflags "$ldflags" ./cmd/tofu
}

check() {
	go test -v ./...
}

package() {
	install -Dm0755 tofu -t "$pkgdir"/usr/bin/
}

sha512sums="
8050f5726ba13f833ddc6ae1d32deca360745bb3a90615b5caeb4de3623d1f52c38399b27ac2c881dc0c9697ad526d61bef38e46cba53e48af09b93308dca7d7  opentofu-1.6.0_alpha5.tar.gz
"
